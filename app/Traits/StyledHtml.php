<?php

namespace App\Traits;


use Illuminate\Support\Str;

trait StyledHtml
{
    protected function styledHtml(string $html)
    {
        $result = preg_replace(
            '/<p><img+/', '<p style="margin-top: 3rem; margin-bottom: 3rem;"><img style="width: min(100%, 500px) !important; height: auto !important; display: block; margin-left: auto; margin-right: auto;" ',
            $html
        );
        $result = preg_replace(
            '/<p><a(.*?)><img+/', '<p style="margin-top: 3rem; margin-bottom: 3rem;"><img style="width: min(100%, 500px) !important; height: auto !important; display: block; margin-left: auto; margin-right: auto;" ',
            $result
        );
        $result = preg_replace(
            '/><\/a><\/p>+/', '></p>',
            $result
        );
        $result = '<div class="kt-infobox__section"><div class="kt-infobox__content kt-infobox__content--md">'
            . $result
            .'</div>';
        return $result;
    }

    protected function blankStyledHtml(string $text)
    {
        return '<div class="kt-infobox__section text-center pt-lg-5 pb-3 pb-lg-0"><h4>'
            . $text
            .'</h4></div> ';
    }

    protected function description(string $text)
    {
        return '<div class="kt-infobox__section p-3 shadow-sm" style="border-radius: 7px; border: rgba(0,0,0,0.12) 1px solid;"><div class="kt-infobox__content kt-infobox__content--md"><p class="mb-0">'
            . $text
            . '</p></div></div>';
    }

    protected function getTableOfContent(string $content)
    {
        $result = [];
        $contentList = [];
        preg_match_all('/<h[1-3]>(.*?)<\/h[1-3]>/', $content, $contentList);
        foreach($contentList[1] as $index => $contentItem) {
            $result[$index] = [
                'hash' => Str::slug($contentItem),
                'title' => $contentItem,
            ];
        }
        return $result;
    }

}

