<?php

namespace App\Traits;

trait CheckVariables
{

    protected function isEmptyString($string)
    {
        return is_null($string) || $string == '';
    }

}
