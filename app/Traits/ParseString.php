<?php

namespace App\Traits;

trait ParseString
{

    protected function getSlug(string $text)
    {
        $result = preg_replace('/[[:space:]]-[[:space:]]+/', '-', $text);
        $result = preg_replace('/[[:space:]]+/', '-', $result);
        return strtolower($result);
    }

    protected function cleanContent($content)
    {
        if ($content == null) {
            $content = '';
        }
        $result = $this->removeSpace($content);
        $result = $this->removeScript($result);
        return $result;
    }

    protected function removeSpace(string $html)
    {
        return preg_replace('/<p>&nbsp;<\/p>+/', '', $html);
    }

    protected function removeScript(string $html)
    {
        $result = preg_replace('/<script>(.*?)<\/script>/', '', $html);
        $result = preg_replace('/<?php(.*?)?>/', '', $result);
        return $result;
    }

}
