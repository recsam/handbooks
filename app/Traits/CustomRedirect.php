<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait CustomRedirect
{
    protected function redirectTo()
    {
        if (Auth::guard('writer')->check()) {
            return route('writer.books');
        } else if (Auth::guard('admin')->check()) {
            return route('admin.dashboard');
        } else {
            return '/';
        }
    }
}
