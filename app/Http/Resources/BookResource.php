<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'book_name' => $this->book_name,
            'cover_image_url' => $this->cover_image_url,
            'description' =>
                is_null($this->description)
                    ? __('No description')
                    : $this->description,
            'updated_at' => $this->updated_at
        ];
    }
}
