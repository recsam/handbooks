<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'page_name' => $this->page_name,
            'access' => $this->access,
            'updated_at' => $this->updated_at,
            'book_name' => $this->book_name,
        ];
    }
}
