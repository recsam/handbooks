<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocalizationController extends Controller
{

    public function setLanguage($locale)
    {
        if (!in_array($locale, config('app.locales'))) {
            return redirect()->back();
        }
        session()->put('locale', $locale);
        App::setLocale($locale);
        return redirect()->back();
    }

}
