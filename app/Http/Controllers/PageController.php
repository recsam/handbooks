<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Traits\StyledHtml;
use Illuminate\Http\Request;

class PageController extends Controller
{
    use StyledHtml;

    public function index(Request $request, $slug)
    {
        $temp_page = Page::where('id', $request->page)->select(['access', 'writer_id'])->first();
        if ($temp_page->access == 'private' && Auth('writer')->id() != $temp_page->writer_id) {
            return view('pages.error.not_found');
        }
        $page = Page::where('id', $request->page)
            ->select(['id', 'page_name', 'heading', 'description', 'content', 'book_id', 'writer_id'])
            ->first();
        $page->description =
            $this->isEmptyString($page->description)
                ? ''
                : $this->description($page->description);
        $tableOfContent = $this->getTableOfContent($page->content ? $page->content : '');
        $page->content =
            $this->isEmptyString($page->content)
                ? $this->blankStyledHtml(__('No content'))
                : $this->styledHtml($page->content);
        return view('pages.guest.page.index')
            ->with('page', $page)
            ->with('table_of_content', $tableOfContent);
    }

}
