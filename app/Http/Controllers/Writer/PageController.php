<?php

namespace App\Http\Controllers\Writer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Writer\CreatePageRequest;
use App\Http\Resources\PageResource;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:writer');
    }

    public function index()
    {
        $pages = Page::belongsToUser()->select('id', 'page_name', 'slug', 'access', 'updated_at', 'book_id')
            ->paginate(10);
        return view('pages.writer.page.index')
            ->with('pages', PageResource::collection($pages));
    }

    public function create(Request $request)
    {
        $page_name = $request->get('page_name', 'Untitled Page');
        $slug = Str::slug($request->page_name);
        $page = Page::create([
            'book_id' => $request->book_id,
            'page_name' => $page_name,
            'heading' => $page_name,
            'slug' => $slug,
            'access' => 'public',
            'writer_id' => auth()->id()
        ]);
        return redirect()->route('writer.pages.edit', ['slug' => $slug, 'page' => $page->id]);
    }

    public function edit(Request $request, $slug)
    {
        if (!$request->has('page')) {
            $page = Page::where('slug', $slug)
                ->first();
            return view('pages.writer.page.edit.index')
                ->with('page', $page);
        }

        $page = Page::findOrFail($request->page);
        return view('pages.writer.page.edit.index')
            ->with('page', $page);
    }

    public function update($id, CreatePageRequest $request)
    {
        $page = Page::findOrFail($id);
        $attributes = [
            'page_name' => $request->page_name == '' ? $request->heading : $request->page_name,
            'heading' => $request->heading,
            'slug' => $request->slug,
            'description' => $request->description,
            'content' => $this->cleanContent($request->inputContent),
            'access' => $request->access,
        ];
        $page->update($attributes);
        if ($book = $page->book()->first()) {
            return redirect()->route('writer.books.detail', $book->id);
        }
        return redirect()->route('writer.pages');
    }

    public function search(Request $request)
    {
        return redirect()->route('writer.pages', ['q' => $request->search]);
    }

    public function restore($id)
    {
        Page::withTrashed()->whereId($id)->restore();
        return redirect()->route('writer.archives');
    }

    public function destroy($id)
    {
        Page::findOrFail($id)->delete();
        return redirect()->route('writer.pages');
    }

    public function forceDelete($id)
    {
        Page::withTrashed()->whereId($id)->forceDelete();
        return redirect()->route('writer.archives');
    }

}
