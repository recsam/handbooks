<?php

namespace App\Http\Controllers\Writer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Writer\CreateBookRequest;
use App\Http\Resources\BookResource;
use App\Http\Resources\PageResource;
use App\Models\Book;
use App\Models\Category;
use App\Models\Language;
use App\Models\Page;
use Illuminate\Http\Request;

class BookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:writer');
    }

    public function index()
    {
        $books = Book::belongsToUser()
            ->select(['id', 'book_name', 'cover_image', 'description', 'updated_at'])
            ->orderBy('book_name')
            ->paginate(12);
        return view('pages.writer.books.index')
            ->with('books', BookResource::collection($books));
    }

    public function create()
    {
        return view('pages.writer.books.create')
            ->with([
                'categories' =>
                    Category::select(['id', 'category_name'])
                        ->orderBy('category_name')
                        ->get(),
                'languages' =>
                    Language::select(['id', 'language_name'])
                        ->orderBy('language_name')
                        ->get()
            ]);
    }

    public function store(CreateBookRequest $request)
    {
        $book = Book::create([
            'book_name' => $request->book_name,
            'writer_id' => auth()->id(),
            'language_id' => $request->language_id,
            'cover_image' => $request->cover_image,
            'access' => 'public',
        ]);
        $book->categories()->attach($request->input('category_id', []));
        return redirect()->route('writer.books.detail', $book->id);
    }

    public function detail($id)
    {
        $pages = Page::where('book_id', $id)
            ->select('id', 'page_name', 'slug', 'access', 'updated_at')
            ->paginate(10);
        return view('pages.writer.books.detail')
            ->with([
                'book' => Book::findOrFail($id),
                'pages' => PageResource::collection($pages)
            ]);
    }

    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $selectedCategories = $book->categories->map(function ($category) {
            return $category->id;
        });
        return view('pages.writer.books.edit')
            ->with([
                'book' => $book,
                'selected_categories' => $selectedCategories,
                'categories' =>
                    Category::select(['id', 'category_name'])
                        ->orderBy('category_name')
                        ->get(),
                'languages' =>
                    Language::select(['id', 'language_name'])
                        ->orderBy('language_name')
                        ->get()
            ]);
    }

    public function update(CreateBookRequest $request, $id)
    {
        $attributes = $request->all();
        $book = Book::findOrFail($id);
        $book->update($attributes);
        $book->categories()->sync($request->input('category_id', []));
        return redirect()->route('writer.books.detail', $id);
    }

    public function addPages($id)
    {
        return view('pages.writer.books.add_page')
            ->with('book', Book::findOrFail($id));
    }

    public function search(Request $request)
    {
        return redirect()->route('writer.books', ['q' => $request->search]);
    }

    public function restore($id)
    {
        Book::withTrashed()->whereId($id)->restore();
        return redirect()->route('writer.archives');
    }

    public function destroy($id)
    {
        Book::destroy($id);
        return redirect()->route('writer.books');
    }

    public function forceDelete($id)
    {
        $book = Book::withTrashed()->whereId($id)->first();
        $book->pages()->update(['book_id' => null]);
        $book->forceDelete();
        return redirect()->route('writer.archives');
    }

}
