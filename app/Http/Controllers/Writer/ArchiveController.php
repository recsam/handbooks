<?php

namespace App\Http\Controllers\Writer;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ArchiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:writer');
    }

    public function index()
    {
        $userId = auth('writer')->id();
        $archived_books = Book::select(
            DB::raw('"books" as type'),
            'id',
            'book_name as name',
            'deleted_at'
        )->where('writer_id', $userId)->onlyTrashed();
        $archived_items = Page::select(
            DB::raw('"pages" as type'),
            'id',
            'page_name as name',
            'deleted_at'
        )->where('writer_id', $userId)->onlyTrashed()
            ->union($archived_books)
            ->orderBy('deleted_at', 'DESC')
            ->paginate(10);
        return view('pages.writer.archives.index')
            ->with('items', $archived_items);
    }

}
