<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookResource;
use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $books = Book::belongsToUser()
            ->select(['id', 'book_name', 'cover_image', 'description', 'updated_at'])
            ->orderBy('book_name')
            ->paginate(12);

        return view('pages.profile.index')
            ->with('user', $user)
            ->with('books', BookResource::collection($books));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        $books = Book::belongsToUser($id)
            ->select(['id', 'book_name', 'cover_image', 'description', 'updated_at'])
            ->orderBy('book_name')
            ->paginate(12);

        return view('pages.profile.index')
            ->with('user', $user)
            ->with('books', BookResource::collection($books));
    }

    public function edit()
    {
        return view('pages.profile.edit');
    }

}
