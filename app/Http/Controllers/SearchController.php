<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function index()
    {
        $items = Book::select(
            DB::raw('"books" as type'),
            'id',
            'book_name as name',
            'updated_at'
        );
        $items = Page::select(
            DB::raw('"pages" as type'),
            'id',
            'page_name as name',
            'updated_at'
        )
            ->union($items)
            ->orderBy('updated_at', 'DESC')
            ->paginate(10);
        return $items;
        return view('pages.guest.search.index');
    }

    public function search(Request $request)
    {
        return redirect()->route('search', ['q' => $request->search]);
    }

}
