<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{

    public function uploadImage(Request $request) {
        return $request->file('image')->store('/images');
    }

}
