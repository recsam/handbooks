<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ErrorController extends Controller
{

    public function notFound()
    {
        return view('pages.error.not_found');
    }

}
