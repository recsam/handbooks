<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        return view('pages.admin.user.index')
            ->with('users',
                User::where('role', 'writer')
                    ->where('name', 'like', '%' . $request->query('q', '') . '%')
                    ->paginate(10)
            );
    }

    public function search(Request $request)
    {
        return redirect()->route('admin.users.index', ['q' => $request->search]);
    }

}
