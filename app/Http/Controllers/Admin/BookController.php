<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $books = Book::select(['id', 'book_name', 'cover_image', 'description', 'updated_at', 'writer_id', 'access'])
            ->withCount('pages')
            ->orderBy('book_name')
            ->paginate(12);
        return view('pages.admin.book.index')
            ->with('books', $books);
    }

    public function search(Request $request)
    {
        $q = $request->search;
        return redirect()->route('writer.books.index', ['q' => $q]);
    }

    public function destroy($id)
    {
        Book::destroy($id);
        return redirect()->route('writer.books');
    }

}
