<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLanguageRequest;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        return view('pages.admin.language.index')
            ->with('languages',
                Language::where('language_name', 'like', '%' . $request->query('q', '') . '%')
                    ->paginate(10)
            );
    }

    public function create()
    {
        return view('pages.admin.language.create');
    }

    public function store(CreateLanguageRequest $request)
    {
        Language::create([
            'language_name' => $request->language_name
        ]);
        return redirect()->route('admin.languages.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        Language::findOrFail($id)->delete();
        return redirect()->route('admin.languages.index');
    }

    public function search(Request $request)
    {
        return redirect()->route('admin.languages.index', ['q' => $request->search]);
    }

}
