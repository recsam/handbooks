<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        return view('pages.admin.category.index')
            ->with('categories',
                Category::where('category_name', 'like', '%' . $request->query('q', '') . '%')
                    ->withCount('books')
                    ->paginate(10)
            );
    }

    public function create()
    {
        return view('pages.admin.category.create');
    }

    public function store(CreateCategoryRequest $request)
    {
        Category::create([
            'category_name' => $request->category_name,
            'description' => $request->description
        ]);
        return redirect()->route('admin.categories.index');
    }

    public function show($id)
    {
        return ['message' => 'View Category'];
    }

    public function edit()
    {

    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        return redirect()->route('admin.categories.index');
    }

    public function search(Request $request)
    {
        return redirect()->route('admin.categories.index', ['q' => $request->search]);
    }

}
