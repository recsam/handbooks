<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Category extends Model
{
    protected $fillable = [
        'category_name',
        'description'
    ];

    public function books()
    {
        return $this->belongsToMany(Book::class, 'category_book');
    }

    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at, config('app.timezone'))
            ->setTimezone('GMT+7')
            ->format('d M Y');
    }
}
