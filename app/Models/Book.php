<?php

namespace App\Models;

use App\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'book_name',
        'writer_id',
        'language_id',
        'cover_image',
        'access',
        'first_page',
        'description'
    ];

    protected $attributes = [
        'description' => '',
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope('book_name'));
    }

    public function writer()
    {
        return $this->belongsTo(User::class, 'writer_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_book');
    }

    public function pages()
    {
        return $this->hasMany('App\Models\Page');
    }

    public function getCoverImageUrlAttribute()
    {
        return
            $this->cover_image
                ? asset('storage/' . $this->cover_image)
                : asset('media/book-cover-placeholder.png');
    }

    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at, config('app.timezone'))
            ->setTimezone('GMT+7')
            ->format('d M Y');
    }

    public function scopeBelongsToUser(Builder $query, $userId = null)
    {
        if ($userId == null) {
            if (!($userId = Auth::id())) {
                return $query->where('writer_id', 1);
            }
        }
        return $query->where('writer_id', $userId);
    }

}
