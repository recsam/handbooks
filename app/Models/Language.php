<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Language extends Model
{
    protected $fillable = [
        'language_name'
    ];

    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at, config('app.timezone'))
            ->setTimezone('GMT+7')
            ->format('d M Y');
    }
}
