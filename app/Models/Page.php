<?php

namespace App\Models;

use App\Scopes\FilterScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Page extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'book_id',
        'writer_id',
        'page_name',
        'heading',
        'slug',
        'description',
        'content',
        'access'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new FilterScope('page_name'));
    }

    public function book()
    {
        return $this->belongsTo('App\Models\Book');
    }

    public function writer()
    {
        return $this->belongsTo('App\Models\User', 'writer_id');
    }

    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at, config('app.timezone'))
            ->setTimezone('GMT+7')
            ->format('d M Y');
    }

    public function getBookNameAttribute()
    {
        $book_name = '';
        if ($this->book_id) {
            if ($book = Book::find($this->book_id)) {
                $book_name = $book->book_name;
            }
        }
        return $book_name;
    }

    public function scopeBelongsToUser(Builder $query, $userId = null)
    {
        if ($userId == null) {
            if (!($userId = Auth::id())) {
                return $query->where('writer_id', 1);
            }
        }
        return $query->where('writer_id', $userId);
    }
}
