<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FilterScope implements Scope
{
    protected $keyword;

    /**
     * FilterScope constructor.
     * @param string $keyword
     */
    public function __construct(string $keyword = '')
    {
        $this->keyword = $keyword;
    }

    public function apply(Builder $builder, Model $model)
    {
        $query = request()->query('q', '');
        return $builder->where($this->keyword, 'like', "%$query%");
    }
}
