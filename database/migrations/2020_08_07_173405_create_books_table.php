<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('book_name');
            $table->unsignedBigInteger('writer_id');
            $table->unsignedSmallInteger('language_id');
            $table->string('cover_image')->nullable();
            $table->enum('access', ['public', 'private']);
            $table->text('description');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('writer_id')->references('id')->on('users');
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
