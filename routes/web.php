<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

Route::group([
    'domain' => 'admin.' . config('app.short_url'),
    'namespace' => 'Auth',
    'as' => 'auth.'
], function () {

    Route::get('/login', 'LoginController@showAdminLoginForm')->name('login.admin');
    Route::get('/register', 'RegisterController@showAdminRegisterForm')->name('register.admin');

});

Route::group([
    'domain' => 'admin.' . config('app.short_url'),
    'namespace' => 'Admin',
    'as' => 'admin.'
], function () {

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::group([
        'prefix' => '/users',
        'as' => 'users.'
    ], function () {

        Route::get('/', 'UserController@index')->name('index');
        Route::post('/search', 'UserController@search')->name('search');

    });

    Route::group([
        'prefix' => '/books',
        'as' => 'books.'
    ], function () {

        Route::get('/', 'BookController@index')->name('index');
        Route::post('/search', 'BookController@search')->name('search');

    });

    Route::resource('/categories', 'CategoryController');
    Route::group([
        'prefix' => '/categories',
        'as' => 'categories.'
    ], function () {

        Route::post('/search', 'CategoryController@search')->name('search');

    });

    Route::resource('/languages', 'LanguageController');
    Route::group([
        'prefix' => '/languages',
        'as' => 'languages.'
    ], function () {

        Route::post('/search', 'LanguageController@search')->name('search');

    });

});

Route::get('/', 'HomeController@index')->name('home');
Route::get('/search', 'SearchController@index')->name('search');
Route::get('/faq', 'FaqController@index')->name('faq');
Route::get('/{slug}/read', 'PageController@index')->name('pages.detail');
Route::get('/set-language/{locale}', 'LocalizationController@setLanguage')->name('languages.set');

Route::middleware('auth:admin,writer')->get('/profiles', 'ProfileController@index')->name('profiles.index');
Route::middleware('auth:writer,admin')->get('/profiles/edit', 'ProfileController@edit')->name('profiles.edit');
Route::get('/profiles/{id}', 'ProfileController@show')->name('profiles.show');

Route::post('/search', 'SearchController@search')->name('search.form');

Route::group([
    'namespace' => 'Auth',
    'as' => 'auth.'
], function () {


    Route::post('/logout', 'LoginController@logout')->name('logout');

    Route::group([
        'prefix' => '/login',
        'as' => 'login.'
    ], function () {

        Route::get('/', 'LoginController@showWriterLoginForm')->name('writer');
        Route::post('/{role}', 'LoginController@login')->name('form');

    });

    Route::group([
        'prefix' => '/register',
        'as' => 'register.'
    ], function () {

        Route::get('/', 'RegisterController@showWriterRegisterForm')->name('writer');
        Route::post('/{role}', 'RegisterController@register')->name('form');

    });

});

Route::group([
    'namespace' => 'Auth',
    'as' => 'password.'
], function () {

    Route::get('/forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('request');
    Route::get('/reset-password/{token}', 'ResetPasswordController@showResetForm')->name('reset');

    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('email');
    Route::post('/password/reset', 'ResetPasswordController@reset')->name('update');

});

Route::group([
    'namespace' => 'Writer',
    'as' => 'writer.'
], function () {

    Route::get('/archives', 'ArchiveController@index')->name('archives');

    Route::get('/all-books', 'BookController@index')->name('books');
    Route::group([
        'prefix' => '/books',
        'as' => 'books.'
    ], function () {

        Route::get('/create', 'BookController@create')->name('create');
        Route::get('/{id}/edit-info', 'BookController@edit')->name('edit');
        Route::get('/{id}', 'BookController@detail')->name('detail');
        Route::get('/add-pages/{id}', 'BookController@addPages')->name('add_pages');

        Route::post('/', 'BookController@store')->name('store');
        Route::post('/search', 'BookController@search')->name('search');
        Route::put('/update/{id}', 'BookController@update')->name('update');
        Route::post('/restore/{id}', 'BookController@restore')->name('restore');
        Route::delete('/{id}', 'BookController@destroy')->name('destroy');
        Route::delete('/force-delete/{id}', 'BookController@forceDelete')->name('force_delete');

    });

    Route::get('/all-pages', 'PageController@index')->name('pages');
    Route::get('/{slug}/edit', 'PageController@edit')->name('pages.edit');
    Route::group([
        'prefix' => '/pages',
        'as' => 'pages.'
    ], function () {

        Route::post('/create', 'PageController@create')->name('create');
        Route::post('/search', 'PageController@search')->name('search');
        Route::put('/update/{id}', 'PageController@update')->name('update');
        Route::post('/restore/{id}', 'PageController@restore')->name('restore');
        Route::delete('/{id}', 'PageController@destroy')->name('destroy');
        Route::delete('/force-delete/{id}', 'PageController@forceDelete')->name('force_delete');

    });

});

Route::post('/uploads/image', 'FileUploadController@uploadImage')->name('uploads.image');

Route::fallback('ErrorController@notFound')->name('not_found');
