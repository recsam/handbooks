<?php

return [

    'home' => [
        'banner' => [
            'title' => 'Find a book?',
            'search' => 'Search here',
        ],
        'feature_cards' => [
            '1' => [
                'title' => 'Get Started',
                'content' => 'Create your own books',
            ],
            '2' => [
                'title' => 'Tutorials',
                'content' => 'Learn from others',
            ],
            '3' => [
                'title' => 'Explore',
                'content' => 'See others\' books',
            ],
        ],
        'actions' => [
            'register' => [
                'title' => 'Become a Writer',
                'content' => 'And share your knowledge to the others',
                'button' => 'REGISTER'
            ],
            'login' => [
                'title' => 'Already a Writer?',
                'content' => 'Log in to continue your progress',
                'button' => 'SIGN IN'
            ],
        ],
        'benefits' => [
            'title' => 'Benefits',
            'readers' => [
                'title' => 'Readers',
                'content' => 'Good reading habit is a very beneficial habit which inculcates knowledge and wisdom in an individual. It does many wonderful things to improve knowledge, confidence, and wisdom. Any person with good reading habits finds that books are his/her best friends. Such a person won’t feel lonely as long as he/she has a book. Books are like the best companion one could have. Besides, reading good books has many beneficial effects on a person. Whenever you read a good book, you gain knowledge and improve your wisdom. There is not anything other than a good book to make you a better and wise man.',
            ],
            'writers' => [
                'title' => 'Writers',
                'content' => 'Linguist Walter Ong (1982) observed that writing is necessary to help the human mind achieve its full potential. Writing, for example, allows the writer to concretize abstract ideas and to “connect the dots in their knowledge,” according to the National Commission on Writing in America’s Schools and Colleges (2003, p. 3). Particular kinds of writing tasks may, indeed, be beneficial to intellectual vitality, creativity, and thinking abilities. A study by Klein and Boals (2001) found, for example, that when adults write about significant life events their memory for such events is improved. ',
            ],
        ],
    ],

    'login' => [
        'banner' => [
            'title' => 'JOIN OUR GREAT COMMUNITY',
            'content' => 'See others\' books and make your own books to share.',
            'button' => 'Get An Account',
        ],
        'form' => [
            'title' => 'Login To Your Account',
            'button' => 'Sign In',
            'remember_me' => 'Remember Me',
            'forget_password' => 'Forget Password ?',
        ],
    ],

];
