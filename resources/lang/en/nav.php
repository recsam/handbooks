<?php

return [

    'archives' => 'Archives',
    'books' => 'Books',
    'categories' => 'Categories',
    'dashboard' => 'Dashboard',
    'languages' => 'Languages',
    'pages' => 'Pages',
    'users' => 'Users',

];
