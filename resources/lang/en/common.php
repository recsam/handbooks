<?php

return [

    'books' => 'Books',
    'pages' => 'Pages',
    'users' => 'Users',
    'categories' => 'Categories',
    'languages' => 'Languages',
    'archives' => 'Archives',

    'by' => 'By',

    'item' => 'Items',
    'book_amount' => 'Books',
    'user_amount' => 'Users',

    'all' => [
        'books' => 'All Books',
        'pages' => 'All Pages',
    ],

    'new' => [
        'book' => 'New Book',
        'page' => 'Add Page',
        'category' => 'Add Category',
        'language' => 'Add Language',
    ],

];
