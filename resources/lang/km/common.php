<?php

return [

    'books' => 'សៀវភៅ',
    'pages' => 'ទំព័រ',
    'users' => 'អ្នកប្រើប្រាស់',
    'categories' => 'ប្រភេទ',
    'languages' => 'ភាសា',
    'archives' => 'វត្ថុបោះបង់ចោល',

    'by' => 'ដោយ',

    'item' => 'វត្ថុ',
    'book_amount' => 'ក្បាល',
    'user_amount' => 'នាក់',

    'all' => [
        'books' => 'សៀវភៅទាំងអស់',
        'pages' => 'ទំព័រទាំងអស់',
    ],

    'new' => [
        'book' => 'បង្កើតសៀវភៅថ្មី',
        'page' => 'បន្ថែមទំព័រ',
        'category' => 'បន្ថែមប្រភេទ',
        'language' => 'បន្ថែមភាសា',
    ],

];
