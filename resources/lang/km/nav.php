<?php

return [

    'archives' => 'បោះបង់ចោល',
    'books' => 'សៀវភៅ',
    'categories' => 'ប្រភេទ',
    'dashboard' => 'ផ្ទាំងមុខ',
    'languages' => 'ភាសា',
    'pages' => 'ទំព័រ',
    'users' => 'អ្នកប្រើប្រាស់',

];
