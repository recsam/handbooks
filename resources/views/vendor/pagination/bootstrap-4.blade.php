@if($paginator->count() > 0)
    <div class="kt-datatable__pager">
        <ul class="kt-datatable__pager-nav">
            @if($paginator->onFirstPage())
                <li>
                    <a class="kt-datatable__pager-link kt-datatable__pager-link--first kt-datatable__pager-link--disabled">
                        <i class="fas fa-angle-double-left"></i>
                    </a>
                </li>
                <li>
                    <a class="kt-datatable__pager-link kt-datatable__pager-link--prev kt-datatable__pager-link--disabled">
                        <i class="fas fa-angle-left"></i>
                    </a>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->url(1) }}"
                       class="kt-datatable__pager-link kt-datatable__pager-link--first">
                        <i class="fas fa-angle-double-left"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}"
                       class="kt-datatable__pager-link kt-datatable__pager-link--prev">
                        <i class="fas fa-angle-left"></i>
                    </a>
                </li>
            @endif
            @foreach($elements as $element)
                @if(is_string($element))
                    <li>
                        <a class="kt-datatable__pager-link kt-datatable__pager-link-number">
                            {{ $element }}
                        </a>
                    </li>
                @endif

                @if(is_array($element))
                    @foreach($element as $page => $url)
                        @if($page == $paginator->currentPage())
                            <li>
                                <a class="kt-datatable__pager-link kt-datatable__pager-link-number kt-datatable__pager-link--active">
                                    {{ $page }}
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ $url }}" class="kt-datatable__pager-link kt-datatable__pager-link-number">
                                    {{ $page }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}"
                       class="kt-datatable__pager-link kt-datatable__pager-link--first">
                        <i class="fas fa-angle-right"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ $paginator->url($paginator->lastPage()) }}"
                       class="kt-datatable__pager-link kt-datatable__pager-link--prev">
                        <i class="fas fa-angle-double-right"></i>
                    </a>
                </li>
            @else
                <li>
                    <a class="kt-datatable__pager-link kt-datatable__pager-link--next kt-datatable__pager-link--disabled">
                        <i class="fas fa-angle-right"></i>
                    </a>
                </li>
                <li>
                    <a class="kt-datatable__pager-link kt-datatable__pager-link--last kt-datatable__pager-link--disabled">
                        <i class="fas fa-angle-double-right"></i>
                    </a>
                </li>
            @endif
        </ul>

        <div class="kt-datatable__pager-info">
            <span class="kt-datatable__pager-detail">
                Showing {{ $paginator->firstItem() }} - {{ $paginator->lastItem() }} of {{ $paginator->total() }}
            </span>
        </div>
    </div>
@endif
