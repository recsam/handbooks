<nav class="navbar navbar-expand-lg navbar-light bg-transparent py-3 fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            {{-- Push the other actions to the Right --}}
            <div class="mr-auto"></div>
            <form class="input-group mr-2" style="width: 280px;">
                <input class="form-control" style="flex: 0 1 238px;" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-primary" type="submit">
                    <i class="fa fa-search text-white"></i>
                </button>
            </form>

            <ul class="navbar-nav mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="#"><i class="fa fa-2x fa-home"></i></a>
                </li>

                @auth('writer')
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="#"><i class="fa fa-2x fa-book"></i></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-2x fa-user"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                @endauth

            </ul>
        </div>
    </div>
</nav>
