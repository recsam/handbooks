@isset($books)

    @push('styles')
        <link rel="stylesheet" href="{{ asset('css/custom/card-list.css') }}">
    @endpush

    @if($books->count())
        <div class="card-list">
            @foreach($books as $book)
                <a href="{{ route('writer.books.detail', $book->id) }}"
                   class="card kt-portlet kt-portlet--height-fluid">
                    <img src="{{ $book->cover_image_url }}" class="w-100 img-fluid" alt="">
                    <div class="card-text">
                        <h4 class="text-dark">{{ $book->book_name }}</h4>
                        <p class="text-dark">{{ $book->description }}</p>
                        <p><small class="text-muted">Last updated on {{ $book->updated_at }}</small></p>
                    </div>
                </a>
            @endforeach
        </div>
        @if($books->hasPages())
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
                 id="kt_apps_user_list_datatable">
                {{ $books->links() }}
            </div>
        @endif
    @else
        @if(preg_match('/profiles/', request()->url()))
            @if($user->id == Auth::id())
                <div style="width: 100%; margin: 8rem 0; text-align: center; font-size: 1.3em;">
                    There are no books! <a href="{{ route('writer.books.create') }}">Create One</a>
                </div>
            @else
                <div style="width: 100%; margin: 8rem 0; text-align: center; font-size: 1.3em;">
                    This person haven't created any books!
                </div>
            @endif
        @else
            <div style="width: 100%; margin: 8rem 0; text-align: center; font-size: 1.3em;">
                There are no books! <a href="{{ route('writer.books.create') }}">Create One</a>
            </div>
        @endif
    @endif

@endisset
