@isset($items)
    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
         id="kt_apps_user_list_datatable">
        <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
            <thead class="kt-datatable__head">
            <tr class="kt-datatable__row" style="left: 0;">
                <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
                <th class="kt-datatable__cell"><span style="width: 200px;">Name</span></th>
                <th class="kt-datatable__cell"><span style="width: 200px;">Type</span></th>
                <th class="kt-datatable__cell"><span style="width: 100px;">Last Updated</span></th>
                <th class="kt-datatable__cell"><span style="width: 160px">Actions</span></th>
            </tr>
            </thead>
            <tbody class="kt-datatable__body" style="">
            @if($items->count())
                @foreach($items as $index => $item)
                    <tr class="kt-datatable__row" style="left: 0;">
                        <td class="kt-datatable__cell"><span style="width: 30px;">{{ $index + 1 }}</span></td>
                        <td class="kt-datatable__cell">
                            <span style="width: 200px;">{{ $item->name }}</span>
                        </td>
                        <td class="kt-datatable__cell">
                            <span style="width: 200px;">
                                @if($item->type === 'books')
                                    <span class="kt-badge kt-badge--info kt-badge--inline">Book</span>
                                @elseif($item->type === 'pages')
                                    <span class="kt-badge kt-badge--success kt-badge--inline">Page</span>
                                @endif
                            </span>
                        </td>
                        <td class="kt-datatable__cell"><span style="width: 100px;">{{ $item->updated_at }}</span></td>
                        <td class="kt-datatable__cell">
                            <span style="width: 160px;">
                                {{--                    <a href="{{ route('pages.detail', $page->slug) }}" class="btn btn-primary text-center"--}}
                                {{--                       style="width: 50px;"><i class="las la-eye"></i></a>--}}
                                {{--                    <a href="{{ route('writer.pages.edit', $page->slug) }}" class="btn btn-success text-center"--}}
                                {{--                       style="width: 50px;"><i class="las la-edit"></i></a>--}}
                                <a href=""
                                   onclick="event.preventDefault(); document.getElementById('restoreItem{{$item->id}}').submit();"
                                   class="btn btn-success text-center" style="width: 50px;">
                                    <i class="la la-undo"></i>
                                </a>
                                <a href=""
                                   onclick="event.preventDefault(); document.getElementById('deleteItem{{$item->id}}').submit();"
                                   class="btn btn-danger text-center" style="width: 50px;"><i
                                        class="la la-trash"></i></a>
                                <form id="restoreItem{{$item->id}}"
                                      action="{{ route('writer.' . $item->type .'.restore', $item->id) }}"
                                      method="POST"
                                      style="display: none;">
                                        @csrf
                                </form>
                                <form id="deleteItem{{$item->id}}"
                                      action="{{ route('writer.' . $item->type . '.force_delete', $item->id) }}"
                                      method="POST"
                                      style="display: none;">
                                    @method('DELETE')
                                    @csrf
                                </form>
                            </span>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                    <td class="kt-datatable__cell">
                        <span class="text-center my-5">
                            @if(request()->has('q'))
                                {{ __('No results') }}
                            @else
                                {{ __('No Items') }}
                            @endif
                        </span>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{ $items->links() }}
    </div>
@endisset
