@isset($pages)
    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
         id="kt_apps_user_list_datatable">
        <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
            <thead class="kt-datatable__head">
            <tr class="kt-datatable__row" style="left: 0;">
                <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
                <th class="kt-datatable__cell"><span style="width: 200px;">Page Name</span></th>
                <th class="kt-datatable__cell"><span style="width: 100px;">Access</span></th>
                <th class="kt-datatable__cell"><span style="width: 100px;">Last Updated</span></th>
                <th class="kt-datatable__cell"><span style="width: 160px">Actions</span></th>
            </tr>
            </thead>
            <tbody class="kt-datatable__body" style="">
            @if($pages->count())
                @foreach($pages as $index => $page)
                    <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                        <td class="kt-datatable__cell"><span style="width: 30px;">{{ $index + $pages->firstItem() }}</span></td>
                        <td class="kt-datatable__cell">
                        <span style="width: 200px;">
                            <div class="kt-user-card-v2">
                                <div class="kt-user-card-v2__details">
                                    <a class="kt-user-card-v2__name" href="{{ route('pages.detail', ['slug' => $page->slug, 'page' => $page->id]) }}">
                                        {{ $page->page_name }}
                                    </a>
                                </div>
                            </div>
                        </span>
                        </td>
                        <td class="kt-datatable__cell">
                        <span style="width: 100px;">
                            @switch($page->access)
                                @case('public')
                                <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">
                                    {{ __('Public') }}
                                </span>
                                @break
                                @case('private')
                                <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">
                                    {{ __('Only Me') }}
                                </span>
                                @break
                            @endswitch
                        </span>
                        </td>
                        <td class="kt-datatable__cell"><span style="width: 100px;">{{ $page->updated_at }}</span></td>
                        <td class="kt-datatable__cell">
                        <span style="width: 160px;">
                            <a style="width: 50px;" href="{{ route('pages.detail', ['slug' => $page->slug, 'page' => $page->id]) }}"
                               class="btn btn-primary text-center">
                                <i class="la la-eye"></i>
                            </a>
                            <a style="width: 50px;" href="{{ route('writer.pages.edit', ['slug' => $page->slug, 'page' => $page->id]) }}"
                               class="btn btn-success text-center">
                                <i class="la la-edit"></i>
                            </a>
                            <a href=""
                               onclick="event.preventDefault(); document.getElementById('deleteBook{{$page->id}}').submit();"
                               class="btn btn-danger text-center" style="width: 50px;"><i class="la la-trash"></i></a>
                            <form id="deleteBook{{$page->id}}"
                                  action="{{ route('writer.pages.destroy', $page->id) }}"
                                  method="POST"
                                  style="display: none;">
                                @method('DELETE')
                                @csrf
                            </form>
                        </span>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                    <td class="kt-datatable__cell">
                        <span class="text-center my-5">
                            @if(request()->has('q'))
                                {{ __('No results') }}
                            @else
                                {{ __('No Pages') }},
                                <a href=""
                                   onclick="event.preventDefault(); document.getElementById('addPageForm').submit();">
                                    {{ __('Create One') }}
                                </a>
                            @endif
                        </span>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{ $pages->links() }}
    </div>

@endisset
