@isset($languages)
    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
         id="kt_apps_user_list_datatable">
        <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
            <thead class="kt-datatable__head">
            <tr class="kt-datatable__row" style="left: 0;">
                <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
                <th class="kt-datatable__cell"><span style="width: 235px;">Language</span></th>
                <th class="kt-datatable__cell"><span style="width: 100px;">Last Updated</span></th>
                <th class="kt-datatable__cell"><span style="width: 110px">Actions</span></th>
            </tr>
            </thead>
            <tbody class="kt-datatable__body" style="">
            @if(count($languages))
                @foreach($languages as $index => $language)
                    <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                        <td class="kt-datatable__cell">
                            <span
                                style="width: 30px;">{{ $index + $languages->firstItem() }}
                            </span>
                        </td>
                        <td class="kt-datatable__cell"><span style="width: 235px;">{{ $language->language_name }}</span>
                        </td>
                        <td class="kt-datatable__cell"><span
                                style="width: 100px;">{{ $language->updated_at }}</span></td>
                        <td class="kt-datatable__cell">
                            <span style="width: 110px;">
                                <a href="" class="btn btn-primary text-center disabled" style="width: 50px;"><i
                                        class="la la-eye"></i></a>
                                <a href="" class="btn btn-danger text-center" style="width: 50px;"
                                   onclick="event.preventDefault(); document.getElementById('deleteLanguage{{ $language->id }}Form').submit();">
                                    <i class="la la-trash"></i>
                                </a>
                                <form id="deleteLanguage{{ $language->id }}Form"
                                      action="{{ route('admin.languages.destroy', $language->id) }}"
                                      method="POST"
                                      style="display: none;">
                                    @csrf
                                    @method('delete')
                                </form>
                            </span>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                    <td class="kt-datatable__cell"><span
                            class="text-center my-5">@if(request()->has('q')) {{ __('No results') }} @else {{ __('No data') }} @endif</span>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{ $languages->links() }}
    </div>
@endisset
