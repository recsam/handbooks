@isset($users)
    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
         id="kt_apps_user_list_datatable">
        <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
            <thead class="kt-datatable__head">
            <tr class="kt-datatable__row" style="left: 0;">
                <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
                <th class="kt-datatable__cell"><span style="width: 200px;">User</span></th>
                <th class="kt-datatable__cell"><span style="width: 235px;">Email</span></th>
                <th class="kt-datatable__cell"><span style="width: 80px;">Book Count</span></th>
                <th class="kt-datatable__cell"><span style="width: 110px">Actions</span></th>
            </tr>
            </thead>
            <tbody class="kt-datatable__body" style="">
            @if(count($users))
                @foreach($users as $index => $user)
                    <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                        <td class="kt-datatable__cell"><span
                                style="width: 30px;">{{ $index + $users->firstItem() }}</span></td>
                        <td class="kt-datatable__cell">
                <span style="width: 200px;">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__pic">
                            <div class="kt-badge kt-badge--xl kt-badge--warning">D</div>
                        </div>
                        <div class="kt-user-card-v2__details">
                            <a class="kt-user-card-v2__name ml-2" href="{{ route('profiles.show', $user->id) }}">{{ $user->name }}</a>
                        </div>
                    </div>
                </span>
                        </td>
                        <td class="kt-datatable__cell">
                <span style="width: 235px;">
                    <div class="kt-user-card-v2">{{ $user->email }}</div>
                </span>
                        </td>
                        <td class="kt-datatable__cell" style="display: none;">
                <span style="width: 100px;">
                    <span class="btn btn-bold btn-sm btn-font-sm  btn-label-warning">Canceled</span>
                </span>
                        </td>
                        <td class="kt-datatable__cell"><span style="width: 80px;">3</span></td>
                        <td class="kt-datatable__cell">
                <span style="width: 110px;">
                    <a href="{{ route('profiles.show', $user->id) }}" class="btn btn-primary text-center"
                       style="width: 50px;"><i class="la la-eye"></i></a>
                    <a href="" class="btn btn-danger text-center" style="width: 50px;"><i class="la la-trash"></i></a>
                </span>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                    <td class="kt-datatable__cell"><span
                            class="text-center my-5">@if(request()->has('q')) {{ __('No results') }} @else {{ __('No data') }} @endif</span>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>

        {{ $users->links() }}

    </div>
@endisset
