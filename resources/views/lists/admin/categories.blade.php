@isset($categories)
    <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
         id="kt_apps_user_list_datatable">
        <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
            <thead class="kt-datatable__head">
            <tr class="kt-datatable__row" style="left: 0;">
                <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
                <th class="kt-datatable__cell"><span style="width: 235px;">Category Name</span></th>
                <th class="kt-datatable__cell"><span style="width: 200px;">Created By</span></th>
                <th class="kt-datatable__cell"><span style="width: 80px;">Book Count</span></th>
                <th class="kt-datatable__cell"><span style="width: 100px;">Last Updated</span></th>
                <th class="kt-datatable__cell"><span style="width: 110px">Actions</span></th>
            </tr>
            </thead>
            <tbody class="kt-datatable__body" style="">
            @if($categories->count())
                @foreach($categories as $index => $category)
                    <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                        <td class="kt-datatable__cell"><span
                                style="width: 30px;">{{ $index + $categories->firstItem() }}</span></td>
                        <td class="kt-datatable__cell">
                <span style="width: 235px;">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__details">
                            <a class="kt-user-card-v2__name"
                               href="{{ route('admin.categories.show', $category->id) }}">{{ $category->category_name }}</a>
                        </div>
                    </div>
                </span>
                        </td>
                        <td class="kt-datatable__cell">
                <span style="width: 200px;">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__pic">
                            <div class="kt-badge kt-badge--xl kt-badge--warning">D</div>
                        </div>
                        <div class="kt-user-card-v2__details">
                            <a class="kt-user-card-v2__name" href="#">Dinnie Joyes</a>
                        </div>
                    </div>
                </span>
                        </td>
                        <td class="kt-datatable__cell"><span style="width: 80px;">{{ $category->books_count }}</span></td>
                        <td class="kt-datatable__cell"><span style="width: 100px;">{{ $category->updated_at }}</span></td>
                        <td class="kt-datatable__cell">
                <span style="width: 110px;">
                    <a href="{{ route('admin.categories.show', $category->id) }}" class="btn btn-primary text-center disabled"
                       style="width: 50px;"><i class="la la-eye"></i></a>
                    <a href="" class="btn btn-danger text-center" style="width: 50px;"
                       onclick="event.preventDefault(); document.getElementById('deleteCategory{{ $category->id }}Form').submit();">
                        <i class="la la-trash"></i>
                    </a>
                    <form id="deleteCategory{{ $category->id }}Form"
                          action="{{ route('admin.categories.destroy', $category->id) }}"
                          method="POST"
                          style="display: none;">
                        @csrf
                        @method('delete')
                    </form>
                </span>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                    <td class="kt-datatable__cell"><span class="text-center my-5">@if(request()->has('q')) {{ __('No results') }} @else {{ __('No data') }} @endif</span></td>
                </tr>
            @endif
            </tbody>
        </table>
        {{ $categories->links() }}
    </div>
@endisset
