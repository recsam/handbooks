<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded"
     id="kt_apps_user_list_datatable">
    <table class="kt-datatable__table" style="display: block; overflow-x: scroll">
        <thead class="kt-datatable__head">
        <tr class="kt-datatable__row" style="left: 0;">
            <th class="kt-datatable__cell"><span style="width: 30px;">#</span></th>
            <th class="kt-datatable__cell"><span style="width: 200px;">Book Name</span></th>
            <th class="kt-datatable__cell"><span style="width: 200px;">Categories</span></th>
            <th class="kt-datatable__cell"><span style="width: 200px;">Writer</span></th>
            <th class="kt-datatable__cell"><span style="width: 80px;">Page Count</span></th>
            <th class="kt-datatable__cell"><span style="width: 100px;">Access</span></th>
            <th class="kt-datatable__cell"><span style="width: 100px;">Last Updated</span></th>
            <th class="kt-datatable__cell"><span style="width: 60px">Actions</span></th>
        </tr>
        </thead>
        <tbody class="kt-datatable__body" style="">
        @foreach($books as $index => $book)
            <tr data-row="30" class="kt-datatable__row" style="left: 0;">
                <td class="kt-datatable__cell"><span style="width: 30px;">{{ $index + $books->firstItem() }}</span></td>
                <td class="kt-datatable__cell">
                <span style="width: 200px;">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__details">
                            <a class="kt-user-card-v2__name" href="">{{ $book->book_name }}</a>
                        </div>
                    </div>
                </span>
                </td>
                <td class="kt-datatable__cell">
                <span style="width: 200px;">
                    @foreach($book->categories as $category)
                        <a href="" class="btn btn-sm btn-font-sm  btn-label-brand">{{{ $category->category_name }}}</a>
                    @endforeach
                </span>
                </td>
                <td class="kt-datatable__cell">
                <span style="width: 200px;">
                    <div class="kt-user-card-v2">
                        <div class="kt-user-card-v2__pic">
                            <img src="{{ asset('media/profile-placeholder.svg')  }}" alt="">
                        </div>
                        <div class="kt-user-card-v2__details">
                            <a class="kt-user-card-v2__name" href="{{ route('profiles.show', $book->writer_id) }}">{{ $book->writer->name }}</a>
                        </div>
                    </div>
                </span>
                </td>
                <td class="kt-datatable__cell"><span style="width: 80px;">{{ $book->pages_count }}</span></td>
                <td class="kt-datatable__cell">
                <span style="width: 100px;">
                    @switch($book->access)
                        @case('public')
                        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">
                            {{ __('Public') }}
                        </span>
                        @break
                        @case('private')
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">
                            {{ __('Only Me') }}
                        </span>
                        @break
                    @endswitch
                </span>
                </td>
                <td class="kt-datatable__cell"><span style="width: 100px;">{{ $book->updated_at }}</span></td>
                <td class="kt-datatable__cell">
                <span style="width: 60px;">
                    <a href="" class="btn btn-primary text-center disabled" style="width: 50px;"><i
                            class="la la-eye"></i></a>
                </span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $books->links() }}
</div>
