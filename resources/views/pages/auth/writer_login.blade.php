@extends('layouts.navbar_app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/writer-login.css') }}">
@endpush

@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v5 kt-login--signin">
        <div
            class="mt-3 kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-login__left">
                <div class="kt-login__wrapper">
                    <div class="kt-login__content">
                        <a class="kt-login__logo" href="{{ url('/') }}">
                            <img style="width: 300px;" class="img-fluid" src="{{ asset('media/register.svg') }}">
                        </a>

                        <h3 class="kt-login__title mt-5">{{ __('pages.login.banner.title') }}</h3>

                        <span class="kt-login__desc">
                            {{ __('pages.login.banner.content') }}
					</span>

                        <div class="kt-login__actions">
                            <a href="{{ route('auth.register.writer') }}">
                                <button type="button" id="kt_login_signup" class="btn btn-outline-success btn-pill">
                                {{ __('pages.login.banner.button') }}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-login__right">
                <div class="kt-login__wrapper bg-white shadow-sm pt-5">
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">{{ __('pages.login.form.title') }}</h3>
                        </div>
                        <div class="kt-login__form mt-5">
                            <form class="kt-form" method="post" action="{{ route('auth.login.form', 'writer') }}">
                                @csrf
                                <div class="form-group">
                                    <input name="email" class="form-control @error('email') is-invalid @enderror"
                                           type="email" placeholder="Email" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input name="password"
                                           class="form-control form-control-last @error('password') is-invalid @enderror"
                                           type="Password" placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="row kt-login__extra">
                                    <div class="col kt-align-left">
                                        <label class="kt-checkbox">
                                            <input type="checkbox"
                                                   name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            {{ __('pages.login.form.remember_me') }}
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col kt-align-right">
                                        <a href="{{ route('password.request') }}" id="kt_login_forgot" class="kt-link">
                                            {{ __('pages.login.form.forget_password') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="kt-login__actions">
                                    <button type="submit" id="kt_login_signin_submit"
                                            class="btn btn-brand btn-pill btn-elevate">
                                        {{ __('pages.login.form.button') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="kt-login__forgot">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Forgotten Password ?</h3>
                            <div class="kt-login__desc">Enter your email to reset your password:</div>
                        </div>
                        <div class="kt-login__form">
                            <form class="kt-form" action="">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Email" name="email"
                                           id="kt_email" autocomplete="off">
                                </div>
                                <div class="kt-login__actions">
                                    <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">
                                        Request
                                    </button>
                                    <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
