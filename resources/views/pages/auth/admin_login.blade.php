@extends('layouts.navbar_app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/admin-login.css') }}">
@endpush

@section('content')
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img style="width: 100px;" class="img-fluid" src="{{ asset('media/login.svg') }}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">Sign In To Admin</h3>
                        </div>
                        <form class="kt-form" method="post" action="{{ route('auth.login.form', 'admin') }}">
                            @csrf
                            <div class="input-group">
                                <input class="form-control @error('email') is-invalid @enderror" type="text"
                                       placeholder="{{ __('Email') }}" name="email" value="{{ old('email') }}">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="input-group">
                                <input class="form-control @error('password') is-invalid @enderror" type="password"
                                       placeholder="Password" name="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="remember"{{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember Me') }}
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col kt-align-right">
                                    <a href="javascript:;" id="kt_login_forgot" class="kt-login__link">Forget Password
                                        ?</a>
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit" id="kt_login_signin_submit"
                                        class="btn btn-brand btn-elevate kt-login__btn-primary">
                                    Sign In
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@stop
