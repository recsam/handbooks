@extends('layouts.navbar_app', [
    'page_key' => 'profile'
])

@push('styles')
    <style>
        .profile-img {
            width: 15rem;
            height: 15rem;
            object-fit: cover;
            border-radius: 10px;
        }
    </style>
@endpush

@section('content')
    <div class="kt-container">
        <div class="py-3 mt-2 kt-portlet kt-portlet--height-fluid-">
            <div class="kt-portlet__body">
                <!--begin::Widget -->
                <div class="kt-widget kt-widget--user-profile-4">
                    <div class="kt-widget__head">
                        <div class="kt-widget__media">
                            <img class="profile-img kt-widget__media kt-hidden-" src="{{ asset('media/profile-placeholder.svg') }}" alt="image">
                        </div>
                        <div class="kt-widget__content">
                            <div class="kt-widget__section">
                                <a href="#" class="kt-widget__username mt-3" style="font-size: 2em;">
                                    {{ $user->name }}
{{--                                    <span style="font-size: 0.8em;">( Jason Blah ) </span>--}}
                                </a>
                                <a class="kt-widget__username" style="margin-top: -1rem !important; font-size: 1em;" href=""><i class="la la-envelope mr-2"></i>{{ $user->email }}</a>
                                <div class="kt-widget__action mt-3">
                                    <a href="#" class="btn btn-icon btn-circle btn-label-facebook">
                                        <i class="socicon-facebook"></i>
                                    </a>
                                    <a href="#" class="btn btn-icon btn-circle btn-label-twitter">
                                        <i class="socicon-twitter"></i>
                                    </a>
                                    <a href="#" class="btn btn-icon btn-circle btn-label-linkedin">
                                        <i class="socicon-linkedin"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Widget -->
            </div>
        </div>
    </div>
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">{{ __('By This Writer') }}</h3>
                <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                <div class="kt-subheader__group" id="kt_subheader_search">
                    <span class="kt-subheader__desc" id="kt_subheader_total"> {{ $books->count() }} {{ __('Books') }} </span>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
        @include('lists.writer.books')
    </div>
@stop
