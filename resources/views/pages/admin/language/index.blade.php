@extends('layouts.dashboard_app', [
    'page_key' => 'languages-list'
])

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ __('common.languages') }} </h3>

    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

    <div class="kt-subheader__group" id="kt_subheader_search">
        <span class="kt-subheader__desc" id="kt_subheader_total"> {{ $languages->total() }} {{ __('common.languages') }} </span>

        <form method="post" action="{{ route('admin.languages.search') }}" class="kt-margin-l-20" id="kt_subheader_search_form">
            @csrf
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input name="search" type="text" class="form-control" placeholder="Search..." id="generalSearch" value="{{ request()->query('q') }}">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span> @include('icons.search') </span>
                </span>
            </div>
        </form>
    </div>
@stop

@section('subheader-toolbar')
    <a href="{{ route('admin.languages.create') }}" class="btn btn-label-brand btn-bold">
        {{ __('common.new.language') }}
    </a>
@stop

@section('content')
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__body kt-portlet__body--fit">

            @include('lists.admin.languages')

        </div>
    </div>
@stop
