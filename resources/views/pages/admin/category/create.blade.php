@extends('layouts.dashboard_app', [
    'page_key' => 'category-add'
])

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/create.css') }}">
@endpush

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ __('Create Category') }} </h3>
@stop

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_projects_add"
                 data-ktwizard-state="step-first">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
                    <!--begin: Form Wizard Form-->
                    <form method="post" action="{{ route('admin.categories.store') }}" class="kt-form"
                          id="kt_projects_add_form">
                        @csrf
                        <div class="kt-wizard-v1__content">
                            <div class="kt-heading kt-heading--md">New Category:</div>
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                                        Category Name
                                                    </label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input name="category_name"
                                                               class="form-control @error('category_name') is-invalid @enderror""
                                                        type="text"
                                                        value="{{ old('category_name') }}">
                                                        @error('category_name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-form-label col-12">Description</label>
                                                    <div class="col-12 mt-2">
                                                        <textarea name="description"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--begin: Form Actions -->
                        <div class="kt-form__actions">
                            <button type="submit"
                                    class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                    data-ktwizard-type="action-next">
                                Create
                            </button>
                        </div>
                        <!--end: Form Actions -->
                    </form>
                    <!--end: Form Wizard Form-->
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('description');
        });
    </script>
@endpush
