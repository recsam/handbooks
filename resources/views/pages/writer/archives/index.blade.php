@extends('layouts.dashboard_app', [
    'page_key' => 'archives'
])

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ __('common.archives') }} </h3>

    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

    <div class="kt-subheader__group" id="kt_subheader_search">
        <span class="kt-subheader__desc" id="kt_subheader_total"> @isset($items) {{ $items->total() }} @else 0 @endisset {{ __('common.item') }} </span>
    </div>
@stop

@section('content')
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__body kt-portlet__body--fit">
            @include('lists.writer.archives')
        </div>
    </div>
@stop
