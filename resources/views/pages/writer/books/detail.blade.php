@extends('layouts.dashboard_app', [
    'page_key' => 'book-details'
])

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ $book->book_name }} </h3>

    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

    <div class="kt-subheader__group" id="kt_subheader_search">
        <span class="kt-subheader__desc" id="kt_subheader_total"> {{ $pages->total() }} {{ __('common.pages') }} </span>

        <form method="post" action="{{ route('admin.books.search') }}" class="kt-margin-l-20"
              id="kt_subheader_search_form">
            @csrf
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input name="search" type="text" class="form-control" placeholder="Search..." id="generalSearch"
                       value="{{ request()->query('q') }}">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span> @include('icons.search') </span>
                </span>
            </div>
        </form>
    </div>
@stop

@section('subheader-toolbar')
    <a href="{{ route('writer.books.edit', $book->id) }}" class="btn btn-icon mr-2">
        <img src="{{ asset('media/edit.png') }}" alt="" style="width: 29px; height: 27px;">
    </a>
    <button class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        @include('icons.add_page')
    </button>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">
        <!--begin::Nav-->

        <ul class="kt-nav">
            <li class="kt-nav__head">
                {{ __('Add Page') }}:
                <i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right"
                   title="Click to learn more..."></i>
            </li>
            <li class="kt-nav__separator"></li>
            <li class="kt-nav__item">
                <a href="" class="kt-nav__link"
                   onclick="event.preventDefault(); document.getElementById('addPageForm').submit();">
                    <i class="kt-nav__link-icon fa fa-plus"></i>
                    <span class="kt-nav__link-text">{{ __('Create New') }}</span>
                </a>
                <form id="addPageForm"
                      action="{{ route('writer.pages.create') }}"
                      method="POST"
                      style="display: none;">
                    @csrf
                    <input name="book_id" value="{{ $book->id }}">
                    <input name="page_name"
                           value="{{ $book->book_name . ' - ' . __('page') . ' ' . ($book->pages->count() + 1) }}">
                </form>
            </li>
            <li class="kt-nav__item kt-nav__item--disabled">
                <a href="{{ route('writer.books.add_pages', $book->id) }}" class="kt-nav__link">
                    <i class="kt-nav__link-icon fa fa-scroll"></i>
                    <span class="kt-nav__link-text">{{ __('Using Existing Pages') }}</span>
                </a>
            </li>
            <li class="kt-nav__separator"></li>
            <li class="kt-nav__foot">
            </li>
        </ul>
        <!--end::Nav-->
    </div>
@stop

@section('content')
    @isset($pages)
        <div class="kt-portlet kt-portlet--height-fluid">
            <div class="kt-portlet__body kt-portlet__body--fit">
                @include('lists.writer.book_pages')
            </div>
        </div>
    @endisset
@stop
