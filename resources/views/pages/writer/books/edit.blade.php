@extends('layouts.dashboard_app', [
    'page_key' => 'book-edit'
])

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/create.css') }}">
@endpush

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ __('Edit Book') }} </h3>
@stop

@section('content')
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid  kt-wizard-v1 kt-wizard-v1--white" id="kt_projects_add"
                 data-ktwizard-state="step-first">
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v1__wrapper">
                    <!--begin: Form Wizard Form-->
                    <form method="post" action="{{ route('writer.books.update', $book->id) }}"
                          enctype="multipart/form-data"
                          class="kt-form mb-4" id="kt_projects_add_form">
                        @csrf
                        @method('PUT')
                        <div class="kt-wizard-v1__content">
                            <div class="kt-heading kt-heading--md">Edit Book:</div>
                            <div class="kt-section kt-section--first">
                                <div class="kt-wizard-v1__form">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="kt-section__body">
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Book Name</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input name="book_name" class="form-control" type="text"
                                                               placeholder="{{ __('Book name') }}"
                                                               value="{{ old('book_name') ? old('book_name') : $book->book_name }}">
                                                        @error('book_name')
                                                        <span class="form-text text-danger">{{ $message }}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Categories</label>
                                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                                        <select name="category_id[]" class="form-control"
                                                                id="selectCategories" multiple>
                                                            @foreach($categories as $category)
                                                                <option
                                                                    value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Language</label>
                                                    <div class="col-lg-4 col-md-9 col-sm-12">
                                                        <select name="language_id" class="form-control">
                                                            <option>Nothing selected</option>
                                                            @foreach($languages as $language)
                                                                <option
                                                                    value="{{ $language->id }}"
                                                                    @if($language->id == $book->language_id) selected @endif>{{ $language->language_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-last row">
                                                    <label class="col-form-label col-lg-3 col-sm-12">Book Cover</label>
                                                    <div class="col-lg-9 col-sm-12">
                                                        <div class="dropzone dropzone-default" id="uploadCoverImage">
                                                            <div class="dropzone-msg dz-message needsclick">
                                                                @csrf
                                                                <h3 class="dropzone-msg-title">Drop an image here or
                                                                    click to upload.</h3>
                                                                {{-- <span class="dropzone-msg-desc">This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.</span>--}}
                                                            </div>
                                                        </div>
                                                        <div id="imagePath" class="d-none"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--begin: Form Actions -->
                        <div class="kt-form__actions">
                            <button onclick="event.preventDefault(); document.getElementById('deleteBook').submit();"
                                    class="btn btn-danger btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                    data-ktwizard-type="action-next">
                                Delete Book
                            </button>
                            <button type="submit"
                                    class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u"
                                    data-ktwizard-type="action-next">
                                Save
                            </button>
                        </div>
                        <!--end: Form Actions -->
                    </form>
                    <!--end: Form Wizard Form-->
                    <form id="deleteBook"
                          action="{{ route('writer.books.destroy', $book->id) }}"
                          method="POST"
                          style="display: none;">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#selectCategories').selectpicker('val', @json($selected_categories));
            $('#uploadCoverImage').dropzone({
                init: function () {
                    this.on('sending', function (file, xhr, formData) {
                        formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
                    });
                },
                url: '{{ route('uploads.image') }}', // Set the url for your upload script location
                paramName: "image", // The name that will be used to transfer the file
                acceptedFiles: '.png,.jpg,.jpeg,.bmp,.gif',
                maxFiles: 1,
                maxFilesize: 2, // MB
                addRemoveLinks: true,
                success: function (file, response) {
                    $('#imagePath').html(`<input name="cover_image" value="${response}">`);
                }
            });
        });
    </script>
@endpush
