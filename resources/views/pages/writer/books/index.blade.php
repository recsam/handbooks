@extends('layouts.dashboard_app', [
    'page_key' => 'books-list'
])

@section('subheader-main')
    <h3 class="kt-subheader__title"> {{ __('common.all.books') }} </h3>

    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

    <div class="kt-subheader__group" id="kt_subheader_search">
        <span class="kt-subheader__desc" id="kt_subheader_total"> {{ $books->total() }} {{ __('common.book_amount') }} </span>

        <form method="post" action="{{ route('writer.books.search') }}" class="kt-margin-l-20"
              id="kt_subheader_search_form">
            @csrf
            <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                <input name="search" type="text" class="form-control" placeholder="Search..." id="generalSearch"
                       value="{{ request()->query('q') }}">
                <span class="kt-input-icon__icon kt-input-icon__icon--right">
                    <span> @include('icons.search') </span>
                </span>
            </div>
        </form>
    </div>
@stop

@section('subheader-toolbar')
    <a href="{{ route('writer.books.create') }}" class="btn btn-label-brand btn-bold">
        {{ __('common.new.book') }}
    </a>
@stop

@section('content')
    @include('lists.writer.books')
@stop
