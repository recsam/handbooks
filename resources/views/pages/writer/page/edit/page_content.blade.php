<div class="tab-pane active" id="pageContentTab" role="tabpanel">
    <div class="kt-form kt-form--label-right">
        <div class="kt-form__body">

            <div class="kt-section">
                <div class="kt-section__body">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Page Heading</label>
                        <div class="col-lg-9 col-xl-6">
                            <input name="heading" class="form-control" type="text" value="{{ $page->heading }}">
                        </div>
                    </div>
                </div>
            </div>

            <div
                class="kt-separator kt-separator--border-dashed kt-separator--portlet-fit kt-separator--space-lg"></div>

            <div class="kt-section kt-section--first">
                <div class="kt-section__body">
                    <div class="row mx-2">
                        <h3 class="kt-section__title kt-section__title-sm">Content:</h3>
                        <textarea name="inputContent">{{ $page->content }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('inputContent');
        });
    </script>
@endpush
