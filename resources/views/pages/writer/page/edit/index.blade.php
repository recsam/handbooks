@extends('layouts.dashboard_app', [
    'page_key' => 'page-new',
    'form_route' => route('writer.pages.update', $page->id),
    'form_method' => 'put'
])

@section('content')
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">

                @include('menu.page_create-tabs')

            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content  kt-margin-t-20">
                @if($errors->any())
                <div class="text-danger mb-5">
                    <h4>Invalid Input:</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {{-- Page Details Tab Content --}}
                @include('pages.writer.page.edit.page_details')

                {{-- Page Content Tab Content --}}
                @include('pages.writer.page.edit.page_content')

            </div>
        </div>
    </div>
@stop

@section('subheader-main')
    <h3 class="kt-subheader__title">
        Page Editor
    </h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <div class="kt-subheader__group" id="kt_subheader_search">
        <span class="kt-subheader__desc" id="kt_subheader_total">
            {{ $page->page_name }}
        </span>
    </div>
@stop

@section('subheader-toolbar')
    <div class="btn-group">
        <button type="submit" class="btn btn-brand btn-bold">
            Save Changes
        </button>
    </div>
@stop
