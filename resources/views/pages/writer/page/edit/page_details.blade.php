<div class="tab-pane" id="pageDetailsTab" role="tabpanel">
    <div class="kt-form kt-form--label-right">
        <div class="kt-form__body">
            <div class="kt-section kt-section--first">
                <div class="kt-section__body">
                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h3 class="kt-section__title kt-section__title-sm">Page Info:</h3>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Page Name</label>
                        <div class="col-lg-9 col-xl-6">
                            <input name="page_name" class="form-control" type="text" value="{{ old('page_name') ? old('page_name') : $page->page_name }}">
                            <span class="form-text text-muted">If not specified, then it'll take the value of the page heading.</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Page Slug</label>
                        <div class="col-lg-9 col-xl-6">
                            <input name="slug" class="form-control" type="text" value="{{ old('slug') ? old('slug') : $page->slug }}">
                            <span class="form-text text-muted">* Slug only includes alphabets, numbers, dashes(-), and underscores(_)</span>
                            @error('slug')
                            <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">Page Description (Optional)</label>
                        <div class="col-lg-9 col-xl-6">
                            <textarea
                                name="description"
                                class="form-control"
                                id="exampleTextarea"
                                rows="3" placeholder="Description...">{{ old('description') ? old('description') : $page->description }}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h3 class="kt-section__title kt-section__title-sm">Access Control:</h3>
                        </div>
                    </div>

                    @php
                        $accesses = [
                            ['public', __('Public')],
                            ['private', __('Only Me')]
                        ];
                    @endphp

                    <div class="form-group row">
                        <label for="selectPageAccess" class="col-xl-3 col-lg-3 col-form-label">Page Access</label>
                        <div class="col-lg-9 col-xl-6">
                            <select name="access" id="selectPageAccess" class="form-control">
                                @foreach($accesses as $access)
                                    <option value="{{ $access[0] }}" @if($access[0] == $page->access) selected @endif>
                                        {{ $access[1] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
