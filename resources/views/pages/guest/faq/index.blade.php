@extends('layouts.navbar_app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/faq.css') }}">
@endpush

@section('content')
    <div class="kt-container  kt-grid__item kt-grid__item--fluid">

        <div class="kt-portlet kt-faq-v1">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Frequently Asked Questions
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="accordion accordion-solid" id="accordionExample1">
                    @for($i = 0; $i < 10; $i++)
                        @include('pages.guest.faq.question_card')
                    @endfor
                </div>
            </div>
        </div>

    </div>
@stop
