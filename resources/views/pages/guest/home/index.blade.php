@extends('layouts.navbar_app')

@section('content')

    {{-- Banner --}}
    @include('pages.guest.home.banner')

    {{-- Feature Cards --}}
    @include('pages.guest.home.feature_cards')

    {{-- Actions --}}
    @if(!auth('writer')->check() && !auth('admin')->check())
        @include('pages.guest.home.actions')
    @endif

    {{-- Benefits --}}
    @include('pages.guest.home.benefits')

@stop
