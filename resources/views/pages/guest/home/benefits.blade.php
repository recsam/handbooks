<div class="kt-container ">
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-infobox">
                <div class="kt-infobox__header">
                    <h2 class="kt-infobox__title">{{ __('pages.home.benefits.title') }}</h2>
                </div>
                <div class="kt-infobox__body">
                    <div class="kt-infobox__section">
                        <h3 class="kt-infobox__subtitle">{{ __('pages.home.benefits.readers.title') }}</h3>
                        <div class="kt-infobox__content">
                            {{ __('pages.home.benefits.readers.content') }}
                        </div>
                    </div>
                    <div class="kt-infobox__section">
                        <h3 class="kt-infobox__subtitle">{{ __('pages.home.benefits.writers.title') }}</h3>
                        <div class="kt-infobox__content">
                            {{ __('pages.home.benefits.writers.content') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
