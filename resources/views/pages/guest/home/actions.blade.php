<div class="kt-container ">
    <div class="row">
        <div class="col-lg-6">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">{{ __('pages.home.actions.register.title') }}</h3>
                            <p class="kt-callout__desc">
                                {{ __('pages.home.actions.register.content') }}
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="{{ route('auth.register.writer') }}" class="btn btn-custom btn-bold btn-upper btn-font-sm btn-primary">
                                {{ __('pages.home.actions.register.button') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="kt-portlet kt-callout">
                <div class="kt-portlet__body">
                    <div class="kt-callout__body">
                        <div class="kt-callout__content">
                            <h3 class="kt-callout__title">{{ __('pages.home.actions.login.title') }}</h3>
                            <p class="kt-callout__desc">
                                {{ __('pages.home.actions.login.content') }}
                            </p>
                        </div>
                        <div class="kt-callout__action">
                            <a href="{{ route('auth.login.writer') }}" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">
                                {{ __('pages.home.actions.login.button') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
