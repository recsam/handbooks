<div class="kt-sc" style="background-image: url('https://www.incimages.com/uploaded_files/image/1920x1080/getty_883231284_200013331818843182490_335833.jpg')">
    <div class="kt-container" style="background: rgba(0, 0, 0, .5)">
        <div class="kt-sc__bottom">
            <h3 class="kt-sc__heading kt-heading kt-heading--center kt-heading--xxl kt-heading--medium" style="color: rgba(255, 255, 255, 0.85) !important;">
                {{ __('pages.home.banner.title') }}
            </h3>
            <form method="post" action="{{ route('search.form') }}" class="kt-sc__form">
                @csrf
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"> @include('icons.search') </span>
                    </div>
                    <input name="search" type="text" class="form-control" placeholder="{{ __('pages.home.banner.search') }}" aria-describedby="basic-addon1">
                </div>
            </form>
        </div>
    </div>
</div>
