@extends('layouts.navbar_app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/page-details.css') }}">
@endpush

@section('content')
    <!-- begin:: hero -->
    <div class="kt-sc-license"
         style="background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSJ85BMUmFkIqu1oJa3klE-fdD6UXawOnCC4g&usqp=CAU')">
        <div class="kt-container ">
            <div class="kt-sc__top">
                @if(!is_null($page->book))
                    <a href="{{ route('writer.books.detail', $page->book->id) }}">
                        <h3 class="kt-sc__title">
                            {{ $page->book->book_name }}
                        </h3>
                    </a>
                @endif
                <div class="kt-sc__nav">
                    <a href="{{ route('profiles.show', $page->writer->id) }}"
                       class="kt-link kt-link--light kt-font-bold">
                        {{ __('common.by') }}: {{ $page->writer->name }}
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: hero -->
    <div style="margin-top: -15rem"></div>
    <!-- begin:: infobox -->
    <div class="kt-grid__item">
        <div class="kt-container ">
            <div class="kt-portlet">
                <div class="kt-portlet__body">
                    <div class="kt-infobox">
                        <div class="kt-infobox__header mt--2">
                            <h1 class="kt-infobox__title" style="font-size: 3.5em !important;">{{ $page->heading }}</h1>
                        </div>
                        <div class="kt-infobox__body">
                            <div class="row">
                                @if(count($table_of_content))
                                    <div class="col-lg-3 order-lg-1 mb-4">
                                        <ul id="tableOfContent"
                                            class="kt-nav kt-nav--bold kt-nav--fit kt-nav--v4 kt-nav--v4--success">
                                            @foreach($table_of_content as $content)
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link"
                                                       href="#{{ $content['hash'] }}">
                                                        <span class="kt-nav__link-text">{{ $content['title'] }}</span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-9 pr-lg-5">
                                        {!! $page->description !!}
                                        {!! $page->content !!}
                                    </div>
                                @else
                                    <div class="col-lg-12">
                                        {!! $page->description !!}
                                        {!! $page->content !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="fb-share-button mt-4" data-href="https://developers.facebook.com/docs/plugins/"
                             data-layout="button" data-size="large"><a target="_blank"
                                                                       href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                                                       class="fb-xfbml-parse-ignore">Share</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: infobox -->
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            function setId() {
                let tableOfContent = @json($table_of_content);
                document.querySelectorAll('.kt-infobox__content h1, .kt-infobox__content h2, .kt-infobox__content h3')
                    .forEach((heading, index) => {
                        heading.id = tableOfContent[index].hash;
                        heading.className = heading.className + ' anchor';
                    })
            }

            function setActiveLink() {
                document.querySelectorAll('#tableOfContent .kt-nav__item').forEach(navItem => {
                    let navLink = navItem.childNodes[1];
                    if (navLink.href === location.href) {
                        navItem.className = 'kt-nav__item active';
                    } else {
                        navItem.className = 'kt-nav__item';
                    }
                });
            }

            $(window).on('hashchange', function (event) {
                document.querySelectorAll('#tableOfContent .kt-nav__item').forEach(navItem => {
                    setActiveLink();
                });
            });

            setId();
            setActiveLink();
        });
    </script>
@endpush
