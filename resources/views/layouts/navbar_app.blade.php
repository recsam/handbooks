@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/custom/home.css') }}">
@endpush

@section('body')
    @include('layouts.components.header.header_mobile')
    <div class="kt-grid kt-grid--hor kt-grid--root min-vh-100">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

                <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
                    @include('layouts.components.header.top_header')
                </div>
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        {{-- Content --}}
                        <div class="kt-grid__item kt-grid__item--fluid">
                            @yield('content')
                        </div>

                    </div>
                </div>

                {{-- Footer --}}
                @include('layouts.components.footer')
            </div>
        </div>
    </div>
@stop
