<link rel="stylesheet" href="{{ asset('css/app.css') }}">

{{-- Fonts --}}
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"><!--end::Fonts -->
<link rel="stylesheet" href="{{ asset('vendor/line-awesome/css/line-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/socicon/css/socicon.css') }}">

{{-- Others --}}
<link rel="stylesheet" href="{{ asset('vendor/perfect-scrollbar/css/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/tether/dist/css/tether.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/dropzone/dist/dropzone.css') }}">

{{-- Theme and App --}}
<link rel="stylesheet" href="{{ asset('themes/css/style.bundle.min.css') }}">

{{-- Favicon --}}
<link rel="shortcut icon" href="{{ asset('media/logo.png') }}"/>

<style>
    .cke {
        width: 100% !important;
    }
    .lds-ring {
        display: inline-block;
        position: relative;
        width: 80px;
        height: 80px;
    }
    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 64px;
        height: 64px;
        margin: 8px;
        border: 8px solid #4093b3 !important;
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: #4093b3 transparent transparent transparent !important;
    }
    .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
    }
    @keyframes lds-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
</style>

@stack('styles')
