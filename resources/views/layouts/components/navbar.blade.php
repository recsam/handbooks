@isset($page_key)

    @auth('writer')
        @php
            $navs = [[
                'text' => __('nav.books'),
                'icon' => 'fas fa-book-open',
                'link' => route('writer.books'),
                'pages' => ['books-list', 'book-new', 'book-details', 'book-add-pages', 'book-edit']
            ], [
                'text' => __('nav.pages'),
                'icon' => 'fas fa-sticky-note',
                'link' => route('writer.pages'),
                'pages' => ['pages-list', 'page-new']
            ], [
                'text' => __('nav.archives'),
                'icon' => 'fas fa-trash',
                'link' => route('writer.archives'),
                'pages' => ['archives']
            ]];
        @endphp
    @endauth

    @auth('admin')
        @php
            $navs = [[
                'text' => __('nav.dashboard'),
                'icon' => 'fas fa-tachometer-alt',
                'link' => route('admin.dashboard'),
                'pages' => ['dashboard']
            ], [
                'text' => __('nav.users'),
                'icon' => 'fas fa-user',
                'link' => route('admin.users.index'),
                'pages' => ['users-list', 'user-add']
            ], [
                'text' => __('nav.books'),
                'icon' => 'fas fa-book-open',
                'link' => route('admin.books.index'),
                'pages' => ['books-list', 'book-add']
            ], [
                'text' => __('nav.categories'),
                'icon' => 'fas fa-swatchbook',
                'link' => route('admin.categories.index'),
                'pages' => ['categories-list', 'category-add']
            ], [
                'text' => __('nav.languages'),
                'icon' => 'fas fa-globe',
                'link' => route('admin.languages.index'),
                'pages' => ['languages-list', 'language-add']
            ]];
        @endphp
    @endauth

    <ul class="kt-menu__nav w-100">
        @foreach($navs as $nav)
            <li class="kt-menu__item @if(in_array($page_key, $nav['pages'])) kt-menu__item--active @endif"
                aria-haspopup="true">
                <a href="{{ $nav['link'] }}" class="kt-menu__link">
                    <span class="kt-menu__link-text">
                        <i class="{{ $nav['icon'] }} mr-3"></i> {{ $nav['text'] }}
                    </span>
                </a>
            </li>
        @endforeach
    </ul>

@endisset
