@include('layouts.components.header.header_mobile')
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

            @include('layouts.components.header.header')
            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                    {{-- Subheader --}}
                    @include('layouts.components.header.subheader')

                    {{-- Content --}}
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        @yield('content')
                    </div>

                </div>
            </div>

            {{-- Footer --}}
            @include('layouts.components.footer')
        </div>
    </div>
</div>
