<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    const KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#3d94fb",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#3d94fb",
                "warning": "#ffb822",
                "danger": "#fd27eb"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendor/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('vendor/dropzone/dist/dropzone.js') }}"></script>
{{--<script src="./assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>--}}
{{--<script src="./assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>--}}
{{--<script src="./assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>--}}
{{--<script src="./assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>--}}
<!--end:: Global Mandatory Vendors -->

<script src="{{ asset('themes/js/scripts.bundle.min.js') }}" type="text/javascript"></script>

<script>
    Dropzone.autoDiscover = false;
</script>

@stack('scripts')

<script>
    $(document).ready(function () {
        $('.kt-scrolltop').mousedown(function () {
            location.hash = '';
        });
        document.getElementById('mainContent').removeAttribute('style');
        let loading = document.getElementById('loading');
        loading.parentNode.removeChild(loading);
    });
</script>
