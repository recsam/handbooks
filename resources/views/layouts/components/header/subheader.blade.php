<div class="kt-subheader kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            @yield('subheader-main')
        </div>

        {{-- Toolbar --}}
        <div class="kt-subheader__toolbar">
            @yield('subheader-toolbar')
        </div>
    </div>
</div>
