<div class="kt-header__top">
    <div class="kt-container ">

        {{-- Logo --}}
        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
            <div class="kt-header__brand-logo">
                <a href="{{ url('/') }}" class="d-flex">
                    <img alt="Logo" style="width: 30px;" class="img-fluid mr-3" src="{{ asset('media/logo.png') }}"/> <h4 class="pt-1">Handbooks</h4>
                </a>
            </div>
        </div>

        <div class="kt-header__topbar">

            {{-- Language Dropdown --}}
            @include('menu.language_options')

            {{-- User Dropdown --}}
            @if(auth()->guard('admin')->check() || auth()->guard('writer')->check() )
                <div class="kt-header__topbar-item kt-header__topbar-item--user ml-3">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
                        <img class="kt-hidden-" alt="Pic"
                             src="{{ asset('media/profile-placeholder.svg') }}"/>
                    </div>

                    @include('menu.top_header_options')

                </div>
            @endauth

        </div>

    </div>
</div>
