<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
    @include('layouts.components.header.top_header')
    <div class="kt-header__bottom">
        <div class="kt-container ">
            <!-- begin: Header Menu -->
            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                    class="la la-close"></i></button>
            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile">
                    @include('layouts.components.navbar')
                </div>
            </div>
            <!-- end: Header Menu -->
        </div>
    </div>
</div>
<!-- end:: Header -->
