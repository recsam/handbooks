<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__brand">
        <a class="kt-header-mobile__logo d-flex" href="{{ url('/') }}">
            <img alt="Logo" style="width: 30px;" class="img-fluid mr-3" src="{{ asset('media/logo.png') }}"/> <h4 class="pt-1">Handbooks</h4>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                class="fas fa-ellipsis-h"></i>
        </button>
    </div>
</div>
