<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title', config('app.name', 'Laravel')) </title>
    @stack('seo')

    {{-- Styles --}}
    @include('layouts.components.styles')

</head>
<body
    class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-subheader--enabled kt-subheader--transparent kt-page--loading">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0&appId=976814342836762&autoLogAppEvents=1"
            nonce="RYHpjDaC"></script>

<div id="loading" class="lds-ring" style="margin-top: 20vh; margin-left: auto; margin-right: auto;">
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
{{-- Content to Display in the Page--}}
<main id="mainContent" style="display: none;">
    @yield('body')
</main>

{{-- Scroll Top --}}
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

@include('layouts.components.scripts')
</body>
</html>
