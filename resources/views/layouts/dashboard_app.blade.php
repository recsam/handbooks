@extends('layouts.app')

@section('body')
    @include('layouts.components.header.header_mobile')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

                @include('layouts.components.header.header')
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch"
                     id="kt_body">
                    @isset($form_route)
                        <form method="post" action="{{ $form_route }}" class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                              id="kt_content">
                            @csrf
                            @isset($form_method)
                                @method($form_method)
                            @endisset
                            {{-- Subheader --}}
                            @include('layouts.components.header.subheader')

                            {{-- Content --}}
                            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                                @yield('content')
                            </div>

                        </form>
                    @else
                        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
                             id="kt_content">

                            {{-- Subheader --}}
                            @include('layouts.components.header.subheader')

                            {{-- Content --}}
                            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                                @yield('content')
                            </div>

                        </div>
                    @endisset
                </div>

                {{-- Footer --}}
                @include('layouts.components.footer')
            </div>
        </div>
    </div>
@stop
