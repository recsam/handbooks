<div
    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
    <!--begin: Head -->
    <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
        <div class="kt-user-card__avatar">
            <img class="kt-hidden-" alt="Pic"
                 src="{{ asset('media/profile-placeholder.svg') }}"/>
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
            <span
                class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
        </div>
        <div class="kt-user-card__name">
            @if(auth('writer')->check())
                {{ auth('writer')->user()->name }}
            @else
                {{ auth('admin')->user()->name }}
            @endif
        </div>
    </div>
    <!--end: Head -->

    <!--begin: Navigation -->
    <div class="kt-notification">
        <a href="{{ auth('writer')->check() ? route('writer.books') : route('admin.dashboard') }}"
           class="kt-notification__item">
            <div class="kt-notification__item-icon">
                <i class="flaticon2-calendar-3 kt-font-success"></i>
            </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title kt-font-bold">
                    Home
                </div>
            </div>
        </a>
        <a href="{{ route('profiles.index') }}"
           class="kt-notification__item">
            <div class="kt-notification__item-icon">
                <i class="flaticon2-calendar-3 kt-font-success"></i>
            </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title kt-font-bold">
                    My Profile
                </div>
                <div class="kt-notification__item-time">
                    Account settings and more
                </div>
            </div>
        </a>
        <a href="" class="kt-notification__item">
            <div class="kt-notification__item-icon">
                <i class="flaticon2-mail kt-font-warning"></i>
            </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title kt-font-bold">
                    My Messages
                </div>
                <div class="kt-notification__item-time">
                    Inbox and tasks
                </div>
            </div>
        </a>
        <a href="" class="kt-notification__item">
            <div class="kt-notification__item-icon">
                <i class="flaticon2-rocket-1 kt-font-danger"></i>
            </div>
            <div class="kt-notification__item-details">
                <div class="kt-notification__item-title kt-font-bold">
                    My Activities
                </div>
                <div class="kt-notification__item-time">
                    Logs and notifications
                </div>
            </div>
        </a>
        <div class="kt-notification__custom kt-space-between">
            <a href="" class="btn btn-label btn-label-brand btn-sm btn-bold"
               onclick="event.preventDefault(); document.getElementById('logoutForm').submit();">
                Sign Out
            </a>
            <form id="logoutForm"
                  action="{{ route('auth.logout') }}"
                  method="POST"
                  style="display: none;">
                @csrf
            </form>
        </div>
    </div>
    <!--end: Navigation -->
</div>
