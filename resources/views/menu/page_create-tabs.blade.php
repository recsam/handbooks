<ul class="nav nav-tabs nav-tabs-space-lg nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand"
    role="tablist">
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#pageDetailsTab" role="tab">
            <i class="flaticon2-calendar-3"></i> Page Details
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#pageContentTab" role="tab">
            <i class="flaticon2-user-outline-symbol"></i> Page Content
        </a>
    </li>
</ul>
