@php

    $languages = [
        ['en', 'English', 'media/english.svg'],
        ['km', 'ភាសាខ្មែរ', 'media/cambodia.svg']
    ];

@endphp

<div class="kt-header__topbar-item kt-header__topbar-item--langs">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
        <span class="kt-header__topbar-icon kt-header__topbar-icon--brand">
            @switch(session()->get('locale'))
                @case('en')
                    <img src="{{ asset('media/english.svg') }}" alt=""/>
                    @break
                @case('km')
                    <img src="{{ asset('media/cambodia.svg') }}" alt=""/>
                    @break
                @default
                    <img src="{{ asset('media/english.svg') }}" alt=""/>
            @endswitch
        </span>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim">
        <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
            @foreach($languages as $language)
                <li class="kt-nav__item @if(session()->get('locale') == $language[0]) kt-nav__item--active @endif">
                    <a href="{{ route('languages.set', $language[0]) }}" class="kt-nav__link">
                    <span class="kt-nav__link-icon">
                        <img src="{{ asset($language[2]) }}" alt=""/>
                    </span>
                        <span class="kt-nav__link-text">{{ $language[1] }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>
